/*
�����:
���� ��������� 

������:
������� ��� ��������� ������� � ������.
������� ��� ��������� ������� � ������. ����� ������� � p, ����� ������ � n. ����� O(n + p), ���. ������ � O(p).
������� 1. � ������� �������-�������;
������� 2. � ������� z-�������.
p <= 30000, n <= 300000.
*/

#include <algorithm>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include <stdio.h>

using namespace std;

#pragma warning (disable:4996)

void PrefixFunction(const string &s, vector<int> &ans) {
    int ans_length = ans.size();
    for (int i = 1; i < ans_length; ++i) {
        int j = ans[i - 1];
        while (j > 0 && s[i] != s[j]) {
            j = ans[j - 1];
        }
        if (s[i] == s[j]) {
            ++j;
        }
        ans[i] = j;
    }
};

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    string str, temp;
    cin >> temp >> str;

    vector<int> ans(str.size() + temp.size() + 1);

    string used = temp + "@" + str;

    PrefixFunction(used, ans);

    int ans_length = ans.size();
    int temp_length = temp.size();
    for (int i = 0; i < ans_length; ++i) {
        if (ans[i] == temp_length) {
            printf("%d\n", i - 2 * temp_length);
        }
    }

    return 0;
}